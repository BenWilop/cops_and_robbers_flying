from __future__ import annotations

import networkx as nx

from time import time
from .player import Player
from random import choice
from .TreeDecomposition import TreeDecomposition


class Robber(Player):
    def __init__(self,
                 graph: nx.Graph,
                 cops_count: int = None,
                 timeout_init: float = None,
                 timeout_step: float = None) -> None:
        """Initialises the robber.

        :param graph: the graph used for the play
        :param cops_count: the number of cops used in the play
        :param timeout_init: the number of seconds the __init__ call is allowed to take
        :param timeout_step: the number of seconds the step call as well as the get_init_position call
        is allowed to take
        """
        super().__init__(graph, cops_count, timeout_init, timeout_step)
        self.tree_dec: TreeDecomposition
        self.tree_dec = TreeDecomposition(graph, cops_count, timeout_init, timeout_step)
        if not self.tree_dec.big_graph:
            self.tree_dec.compute_tree_decomposition()
        # use left over time to calculate node_distances
        while (time() - self.tree_dec.starting_time < 0.8 * self.timeout_init):
            self.tree_dec.calculate_node_distances()

    def get_init_position(self, cop_positions: list[int]) -> int:
        """Computes the initial robber position based on the initial cop positions.

        Therefor, this method writes the given cop positions to self.cop_positions,
        computes the initial robber position, writes it to self.robber_position,
        and then returns the computed position.

        :param cop_positions: the initial cop positions
        :return: the initial robber position
        """
        # search clique with most cops in it, then pick the one furthest away
        self.cop_positions = cop_positions
        self.tree_dec.cops_pos = self.cop_positions
        self.tree_dec.old_cops_pos = self.cop_positions
        if self.tree_dec.big_graph:
            self.robber_position = self.tree_dec.init_robber_big_graph()
        else:
            self.robber_position = self.tree_dec.init_robber()
        self.tree_dec.robber_pos = self.robber_position
        return self.robber_position


    def step(self, cop_positions: list[int]) -> int:
        """Computes the next robber position based on the current cop positions.

        Therefor, this method writes the given cop positions to self.cop_positions,
        computes a new robber position, writes it to self.robber_position,
        and then returns this position.

        :param cop_positions: the current cop positions
        :return: the next robber position
        """
        # maximize average distance to all cops
        self.cop_positions = cop_positions
        self.tree_dec.old_cops_pos = self.tree_dec.cops_pos
        self.tree_dec.cops_pos = self.cop_positions
        self.robber_position = self.tree_dec.step_robber()
        self.tree_dec.robber_pos = self.robber_position
        return self.robber_position