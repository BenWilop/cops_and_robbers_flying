import networkx as nx
from time import time

from random import sample
from itertools import combinations
from random import randint
from random import choice
# used for calculating tree-width decomposition
from networkx.algorithms import chordal_graph_cliques, complete_to_chordal_graph
from networkx.algorithms import is_connected
import logging
logger = logging.getLogger('group7')

class TreeDecomposition:
    # TODO Block multiple cliques if possible, change self.clique_index_cops to list
    # TODO Better placement strategies, f.e. 1 cop per clique except 1 so robber may get placed there
    # TODO optimize init for multiple components

    def __init__(self, graph: nx.Graph, cops_count: int, timeout_init: float, timeout_step: float):
        self.starting_time = time()  # called at the start of a few methods to track left time
        self.graph = graph
        self.cops_count = cops_count
        self.timeout_init = timeout_init
        self.timeout_step = timeout_step
        self.cops_pos = []
        self.robber_pos = 0
        self.old_cops_pos = self.cops_pos  # pos of cops in last move

        # relabeled graph
        self.int_to_node_dict = {}
        for i in range(self.graph.number_of_nodes()):
            self.int_to_node_dict[i] = list(self.graph.nodes)[i]
        self.node_to_int_dict = {v: k for k, v in self.int_to_node_dict.items()}

        # parameters for tree width-decomposition
        self.tree_width = 0 # tree-width of networkx method, require 1 more cop for safe win
        self.clique = [] # nodes of the decompositioned tree
        self.sepset = [] # overlap between nodes of the decompositioned tree
        self.clique_index_cops = 0 # clique of cops
        self.clique_index_robber = 0 # clique of robber

        # big graph strategy
        # used if graph is too big for tree-width strategy
        self.big_graph = (self.graph.number_of_nodes()*self.graph.number_of_edges() / 1000 > self.timeout_init)
        self.huge_graph = (self.graph.number_of_nodes()*self.graph.number_of_edges() / 100000 > self.timeout_init)

        # if graph is not connected we use big-graph strategies aswell
        self.big_graph = self.big_graph or ( not is_connected(self.graph) )

        if not self.huge_graph:
            # variables for multiple rounds
            # node_distances[i][j] is minimal distance from node i to node j
            self.node_distances = [[0 for i in range(self.graph.number_of_nodes())]
                                   for i in range(self.graph.number_of_nodes())]
            self.node_distances_index = [0, 0] # keeps track of current calculated index for calculate_node_distances




    #################################################################
    #                        tree-width                             #
    #################################################################

    # calculated the tree width and corresponding tree, saves results in parameters for tree width-decomposition
    def compute_tree_decomposition(self) -> None:
        # (ALMOST) THE CODE OF nx.junction_tree(G)
        G = self.graph
        clique_graph = nx.Graph()
        chordal_graph, _ = complete_to_chordal_graph(G)
        self.clique = [tuple(sorted(i)) for i in chordal_graph_cliques(chordal_graph)]
        clique_graph.add_nodes_from(self.clique, type="clique")

        for edge in combinations(self.clique, 2):
            set_edge_0 = set(edge[0])
            set_edge_1 = set(edge[1])
            if not set_edge_0.isdisjoint(set_edge_1):
                sepset = tuple(sorted(set_edge_0.intersection(set_edge_1)))
                clique_graph.add_edge(edge[0], edge[1], weight=len(sepset), sepset=sepset)
                self.sepset.append(sepset)

        junction_tree = nx.maximum_spanning_tree(clique_graph)
        for edge in list(junction_tree.edges(data=True)):
            junction_tree.add_node(edge[2]["sepset"], type="sepset")
            junction_tree.add_edge(edge[0], edge[2]["sepset"])
            junction_tree.add_edge(edge[1], edge[2]["sepset"])
            junction_tree.remove_edge(edge[0], edge[1])

        self.tree_decomposition = junction_tree
        self.tree_width = max([len(node) for node in self.clique]) - 1
        # self.log_stats()


    #################################################################
    #                        cop methods                            #
    #################################################################

    def init_cops(self):
        '''
        place cops on biggest clique
        :return: the initial cop positions
        '''
        index_clique = [(i, s) for i, s in enumerate(self.clique)]
        self.clique_index_cops, max_clique_nodes, _ = \
            max([(i, clq, len(clq)) for i, clq in index_clique], key=lambda item: item[2])

        for cop_index in range(self.cops_count):
            if cop_index < len(max_clique_nodes):
                self.cops_pos.append(max_clique_nodes[cop_index])
            else:
                self.cops_pos.append(max_clique_nodes[0])
        return self.cops_pos


    # used if big_graph = True
    def init_cops_big_graph(self):
        '''
        rank each node with the following formula:
        degree
        :return: the initial cop positions
        '''
        nodes_ranked = sorted(self.graph.degree, key=lambda x: x[1], reverse=True)

        for i in range(self.cops_count):
            self.cops_pos.append(nodes_ranked[i][0])
        return self.cops_pos


    def step_cops(self):
        '''
        search for the first clique on the path towards the clique of the robber
        then  keep the cops on the intersection and move the rest towards that clique
        :return: the new cop positions
        '''
        # do nothing if robber got caught
        if self.robber_pos in self.cops_pos:
            return self.cops_pos

        # if tree_width is not guaranteed to work use method for big_graphs
        if self.tree_width + 1 > self.cops_count:
            return self.step_cops_big_graph()

        # search for clique of robber
        else:
            for clique_index in range(len(self.clique)):
                if self.robber_pos in list(self.clique[clique_index]):
                    self.clique_index_robber = clique_index

            # search for neighbour clique of cops towards clique of robber
            try:
                shortest_path = nx.shortest_path(self.tree_decomposition, self.clique[self.clique_index_cops],
                                 self.clique[self.clique_index_robber])
            except: #if no path to robber exists, stay (not expected to happen since flying cops is always connected)
                return self.cops_pos

            self.clique_index_cops = self.clique.index(shortest_path[2])
            intersection = list(shortest_path[1])
            new_nodes = list(set(shortest_path[2]) - set(intersection))

            # cops in intersection don't move, other cops move to first free tile in new clique (i.e. new_nodes)
            old_cop_pos = self.cops_pos.copy()
            moving_index = 0
            for i in range(self.cops_count):
                if not self.cops_pos[i] in intersection:
                    # redundant cops get placed on last node
                    self.cops_pos[i] = new_nodes[min(moving_index, len(new_nodes)-1)]
                    moving_index += 1

            # if no cops moved (i.e. tree_width >= cops_count) we move the first cop to the pos of robber
            if old_cop_pos == self.cops_pos:
                self.cops_pos[0] = self.robber_pos

            return self.cops_pos



    # used if big_graph = True
    def step_cops_big_graph(self):
        '''
        first cop on old robber pos
        cops on neighbors of robbers
        rest moves random with p = 1/2
        :return: the new cop positions
        '''
        neighbours = list(self.graph[self.robber_pos])
        for i in range(self.cops_count):
            if i == 0:
                self.cops_pos[i] = self.robber_pos
            elif i <= len(neighbours):
                self.cops_pos[i] = neighbours[i-1]
            else:
                if randint(0, 1) == 1:
                    self.cops_pos[i] = choice(list(self.graph.nodes))
        return self.cops_pos



    #################################################################
    #                      robber methods                           #
    #################################################################

    def init_robber(self):
        '''
        search clique with most cops in it, then pick the one furthest away
        :return: the initial robber position
        '''
        self.starting_time = time()
        # compute clique with most cops
        most_cops_clique_index = 0
        most_cops_count = 0
        for i in range(len(self.clique)):
            i_cop_count = 0
            node_list = list(self.clique)[i]
            for cop in self.cops_pos:
                if cop in node_list:
                    i_cop_count += 1
            if i_cop_count > most_cops_count:
                most_cops_count = i_cop_count
                most_cops_clique_index = i

        # rank cliques after the distance to above clique descending
        f = lambda x: self.clique_dist(x, most_cops_clique_index)
        ranked_cliques_index = sorted([i for i in range(len(self.clique))], key=f, reverse=True)

        # choose first node in first clique with a free node, if it doesn't exist cops_count = number of nodes
        found = False
        for i in range(len(self.clique)):
            if not found:
                for node in self.clique[ranked_cliques_index[i]]:
                    if not node in self.cops_pos:
                        self.robber_pos = node
                        found = True
        return self.robber_pos

    def clique_dist(self, node, most_cops_clique_index):
        try:
            dist = len(nx.shortest_path(self.tree_decomposition,
                                        self.clique[most_cops_clique_index], self.clique[node]))
        except:
            dist = float('inf')
        return dist

    # used if big_graph = True
    def init_robber_big_graph(self):
        '''
        choose node with highest average distance to all cops
        :return: the initial robber position
        '''
        self.starting_time = time()
        max_average_distance = 0
        node_index = 0
        while (time() - self.starting_time < 0.95 * self.timeout_step) and node_index < self.graph.number_of_nodes():
            if not self.f(node_index) in self.cops_pos:
                cur_node = self.f(node_index)
                cur_avg_dist = self.avg_distance(cur_node)
                if cur_avg_dist >= max_average_distance:
                    max_average_distance = cur_avg_dist
                    self.robber_pos = cur_node
            node_index += 1
        return self.robber_pos


    # used for big and small graphs
    def step_robber(self):
        '''
        maximize average distance to all cops
        :return: the new robber position
        '''
        # calculate available nodes for robber and find node with highest average distance to all cops
        # to counter tree decomposition ideas
        # improves iterativly for timeouts
        # depth first search
        # TODO instead of depth first search always use the current best node as the starting point
        # cops that didn't move
        blocked_nodes = [self.cops_pos[i] for i in range(self.cops_count) if self.cops_pos[i] == self.old_cops_pos[i]]
        available_nodes = [self.robber_pos]
        visited_nodes = []
        max_avg_dist = 0
        self.starting_time = time()
        while available_nodes and (time() - self.starting_time < 0.95*self.timeout_step):
            visited_nodes.append(available_nodes.pop())
            for adj_node in list(self.graph[visited_nodes[-1]]):
                if (not adj_node in visited_nodes) \
                        and (not adj_node in blocked_nodes): # can't move through standing cops
                    available_nodes.append(adj_node)
                    cur_avg_dist = self.avg_distance(adj_node)
                    if cur_avg_dist >= max_avg_dist and not adj_node in self.cops_pos: # don't land on new cop position
                        max_avg_dist = cur_avg_dist
                        self.robber_pos = adj_node
        return self.robber_pos




    #################################################################
    #                      support methods                          #
    #################################################################

    # used to solve the mapping problem between nodes, that are not enumerated with natural numbers and
    # the set {1, ..., n} for our distance matrix node_distances
    def f(self, index):
        '''
        converts node-index to node
        :param int: index in node_distances of which we want to know the corresponding node
        :return: node of self.graph
        '''
        return self.int_to_node_dict[index]

    def f_inv(self, node):
        '''
        converts node to node-index
        :param node: node of which we want to know the index in node_distances
        :return: index in node_distances
        '''
        return self.node_to_int_dict[node]


    def calculate_node_distances(self):
        """
        calculates the distance between one new node pair to fill node_distances
        so we can save time in step_robber with avg_distance
        """
        if self.huge_graph:
            pass
        else:
            [i, j] = self.node_distances_index
            if [i, j] == [self.graph.number_of_nodes() - 1, self.graph.number_of_nodes() - 1]:
                self.starting_time = 0
            else:
                try:
                    route = nx.shortest_path(self.graph, self.f(i), self.f(j))
                except:
                    route = None
                if route is None:
                    self.node_distances[i][j] = float('inf')
                else:
                    self.node_distances[i][j] = len(route)
                self.node_distances[j][i] = self.node_distances[i][j]
                if j == self.graph.number_of_nodes() - 1:
                    self.node_distances_index[0] += 1
                    self.node_distances_index[1] = 0
                else:
                    self.node_distances_index[1] += 1



    def avg_distance(self, robber_position):
        """
        computes average distance to cops that didn't move in last step on graph as if cops don't block paths
        uses already calculated node_distances and does bfs if a distance wasn't calculated yet
        :param robber_position:
        :return: average distance to cops that didn't move in last step
        """
        if self.huge_graph:
            return self.avg_distance_fast(robber_position)
        else:
            dist_sum = 0
            # cops that didn't move
            blocked_nodes = [self.cops_pos[i] for i in range(self.cops_count)
                             if self.cops_pos[i] == self.old_cops_pos[i]]
            for cop_position in blocked_nodes:
                # check if distance is already saved in node_distances
                if robber_position == cop_position: #shouldn't happen since these pos get dismissed in step_robber
                    logger.info("FAIL, robber_position == cop_position")
                elif self.node_distances[self.f_inv(cop_position)][self.f_inv(robber_position)] == 0:
                    route = nx.shortest_path(self.graph, cop_position, robber_position)
                    if route is None:
                        dist = float('inf')
                    else:
                        dist = len(route)
                    self.node_distances[cop_position][robber_position] = dist
                    dist_sum += dist
                else:
                    try :
                        dist_sum += len(nx.shortest_path(self.graph, cop_position,
                                 robber_position))
                    except: # if no path exists this is true for all reachable robber positions
                        pass
            return dist_sum / self.cops_count



    # used for huge graphs in step_robber
    def avg_distance_fast(self, robber_position):
        """
        computes average distance to cops that didn't move in last step on graph as if cops don't block paths
        by calculating distance to a sample of n cops
        :param robber_position:
        :return: average distance to cops that didn't move in last step
        """
        n = 5
        dist_sum = 0
        for cop_position in sample(self.cops_pos, min(self.cops_count, n)):
            try:
                dist_sum += len(nx.shortest_path(self.graph, cop_position,
                             robber_position))
            except:
                pass
        return dist_sum / min(self.cops_count, n)



    #################################################################
    #                      logging                                  #
    #################################################################

    # used to log graph statistics
    def log_stats(self):

        logger.info("--------------------------")
        logger.info("stats of TreeDecomposition")
        logger.info("--------------------------")
        logger.info("graph:")
        logger.info(self.graph.nodes)
        logger.info(self.graph.edges)
        logger.info("big_graph:")
        logger.info(self.big_graph)
        logger.info("huge_graph:")
        logger.info(self.huge_graph)
        logger.info("")
        logger.info("tree Decomposition:")
        logger.info(self.tree_decomposition.nodes)
        logger.info(self.tree_decomposition.edges)
        logger.info("")
        logger.info("tree clique:")
        logger.info(self.clique)
        logger.info("tree sepset:")
        logger.info(self.sepset)
        logger.info("tree width:")
        logger.info(self.tree_width)
        logger.info("")
        logger.info("")