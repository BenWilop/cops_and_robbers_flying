from __future__ import annotations

import networkx as nx
from .TreeDecomposition import TreeDecomposition


class Player:
    def __init__(self,
                 graph: nx.Graph,
                 cops_count: int = None,
                 timeout_init: float = None,
                 timeout_step: float = None) -> None:
        """Initialises the player.

        This method writes the parameters to instance variables.
        It also initialises the variables cop_positions and robber_position that players can use
        to remember the current positions in the game.

        :param graph: the graph used for the play
        :param cops_count: the number of cops used in the play
        :param timeout_init: the number of seconds the __init__ call is allowed to take
        :param timeout_step: the number of seconds the step call as well as the computation of the initial position(s)
        is allowed to take
        """
        self.graph: nx.Graph = graph
        self.cops_count: int | None = cops_count
        self.timeout_init: float | None = timeout_init
        self.timeout_step: float | None = timeout_step
        self.cop_positions: list[int] | None = None
        self.prev_cop_positions: list[int] | None = None
        self.robber_position: int | None = None