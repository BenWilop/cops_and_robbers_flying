from __future__ import annotations

import networkx as nx

from time import time
from .player import Player
from random import choice
from .TreeDecomposition import TreeDecomposition


class Cops(Player):
    def __init__(self,
                 graph: nx.Graph,
                 cops_count: int = None,
                 timeout_init: float = None,
                 timeout_step: float = None) -> None:
        """Initialises the cops.

        :param graph: the graph used for the play
        :param cops_count: the number of cops used in the play
        :param timeout_init: the number of seconds the __init__ call is allowed to take
        :param timeout_step: the number of seconds the step call as well as the get_init_positions call
        is allowed to take
        """
        super().__init__(graph, cops_count, timeout_init, timeout_step)
        self.tree_dec: TreeDecomposition
        self.tree_dec = TreeDecomposition(graph, cops_count, timeout_init, timeout_step)
        if not self.tree_dec.big_graph:
            self.tree_dec.compute_tree_decomposition()

        # use left over time to calculate node_distances
        # while time() - self.tree_dec.starting_time < 0.95 * self.timeout_init:
        #    self.tree_dec.calculate_node_distances()

    def get_init_positions(self) -> list[int]:
        """Computes the initial cop positions.

        The initial positions are saved in self.cop_positions and then returned.

        :return: the initial cop positions
        """

        if self.tree_dec.big_graph:
            self.cop_positions = self.tree_dec.init_cops_big_graph()
        else:
            # place cops on biggest clique
            self.cop_positions = self.tree_dec.init_cops()
        self.tree_dec.cops_pos = self.cop_positions
        return self.cop_positions


    def step(self, robber_position: int) -> list[int]:
        """Computes the next cop positions based on the current robber position.

        Therefor, the position in self.robber_position is set to the new robber position,
        the positions in self.cop_positions are updated,
        and then they are returned.

        :param robber_position: the current robber position
        :return: the next cop positions
        """

        self.robber_position = robber_position
        self.tree_dec.robber_pos = self.robber_position
        if self.tree_dec.big_graph:
            self.cop_positions = self.tree_dec.step_cops_big_graph()
        else:
            # use tree-width strategy
            self.cop_positions = self.tree_dec.step_cops()
        self.tree_dec.cops_pos = self.cop_positions
        return self.cop_positions

