from __future__ import annotations

import json
import logging
import time
import traceback

import networkx as nx
from networkx.readwrite import json_graph
from os import listdir
from os.path import isfile, join


from src import current_flying as Try_catch_current

game_logger = logging.getLogger('game')


class Game:
    def __init__(self,
                 graph: nx.Graph,
                 robber_engine,
                 cops_engine,
                 cops_count: int = None,
                 timeout_init: float = None,
                 timeout_step: float = None) -> None:
        """Constructor for the Game class

        The Robber class is available via robber_engine.Robber
        and the Cops class is available via cops_engine.Cops.
        The constructor initialises both Cops and Robber and it also computes the 0th round,
        i.e., it uses the cops engine to compute the initial cop positions.

        :param graph: the graph for the game
        :param robber_engine: engine used for the robber
        :param cops_engine: engine used for the cops
        :param cops_count: number of cops in the game
        :param timeout_init: number of seconds the __init__ calls for Cops and Robber
         are allowed to take
        :param timeout_step: number of seconds the step calls (and get_init_position(s))
         for Cops and Robber are allowed to take
        """
        self.graph: nx.Graph = graph
        self.__robber_position = None
        self.__cop_positions = None
        self.__prev_cop_positions = None
        self.__round_number = 0
        self.__next_player = 'cops'
        self.__result = 0
        self.__status = 'Game continues'
        self.timeout_init = timeout_init
        self.timeout_step = timeout_step
        self.cops_count = cops_count

        self.cops = None
        try:
            self.__init_cops(cops_engine)
        except RuntimeError:
            return

        self.robber = None
        try:
            self.__init_robber(robber_engine)
        except RuntimeError:
            return

        try:
            self.__init_cop_positions()
        except RuntimeError:
            return

        try:
            self.__init_robber_position()
        except RuntimeError:
            return

    def __init_cops(self, cops_engine) -> None:
        """Initialises self.cops.

        If an exception is raised during the initialisation, the exception is logged,
        robber wins and a RuntimeError is raised.
        If the timeout for the init phase is exceeded, then robber wins and a RuntimeError is raised.

        :param cops_engine: the engine to use to initialise Cops
        """
        start_time: float = time.process_time()
        try:
            self.cops = cops_engine.Cops(nx.Graph(self.graph),
                                         cops_count=self.cops_count,
                                         timeout_init=self.timeout_init,
                                         timeout_step=self.timeout_step)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__cops_exception()
            raise RuntimeError

        end_time: float = time.process_time()
        if self.__is_init_timeout(start_time, end_time):
            self.__cops_timeout()
            raise RuntimeError

    def __init_robber(self, robber_engine) -> None:
        """Initialises self.robber.

        If an exception is raised during the initialisation, the exception is logged,
        cops win and a RuntimeError is raised.
        If the timeout for the init phase is exceeded, then the cops win and a RuntimeError is raised.

        :param robber_engine: the engine to use to initialise Robber
        """
        start_time: float = time.process_time()
        try:
            self.robber = robber_engine.Robber(nx.Graph(self.graph),
                                               cops_count=self.cops_count,
                                               timeout_init=self.timeout_init,
                                               timeout_step=self.timeout_step)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__robber_exception()
            raise RuntimeError

        end_time: float = time.process_time()
        if self.__is_init_timeout(start_time, end_time):
            self.__robber_timeout()
            raise RuntimeError

    def __init_cop_positions(self) -> None:
        """Computes the initial cop positions and writes them to self.__cop_positions

        If an exception is raised or the step timeout is exceeded,
        then robber wins and a RuntimeError is raised.
        In case of an exception, the exception is logged.
        """
        start_time: float = time.process_time()
        try:
            cop_positions = self.cops.get_init_positions()
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__cops_exception()
            raise RuntimeError
        end_time: float = time.process_time()
        if self.__is_step_timeout(start_time, end_time):
            self.__cops_timeout()
            raise RuntimeError
        self.__set_cop_positions(cop_positions)

    def __init_robber_position(self) -> None:
        """Computes the initial robber position and writes it to self.__cop_positions

        If an exception is raised or the step timeout is exceeded,
        then cops win and a RuntimeError is raised.
        In case of an exception, the exception is logged.
        """
        start_time: float = time.process_time()
        try:
            robber_position = self.robber.get_init_position(self.__cop_positions)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__robber_exception()
            raise RuntimeError
        end_time: float = time.process_time()
        if self.__is_step_timeout(start_time, end_time):
            self.__robber_timeout()
            raise RuntimeError
        self.__set_robber_position(robber_position)

    def next_round(self) -> None:
        """Computes a single round in the game.

        Each round (>0) consists of two steps, one for the robber and one for the cops.
        Thus, this method simply computes two steps.
        """
        self.step()
        self.step()

    def step(self) -> None:
        """Computes a single step in the game.

        Therefor, this method calls __robber_step or __cops_step based on the contents of __next_player.
        In both cases, __next_player is set to the other player after the step.
        If __robber_step is to be called, the round number is increased by one before computing the step.
        """
        if self.__next_player == 'robber':
            self.__robber_step()
            self.__next_player = 'cops'
        elif self.__next_player == 'cops':
            self.__round_number += 1
            self.__cops_step()
            self.__next_player = 'robber'

    def __robber_step(self) -> None:
        """Computes a single robber step by calling the robber's step method.

        We track the time needed to compute the step, and we also look out for exceptions.
        In case of an exception, the exception is logged and the cops win.
        In case of a timeout, the result of the robber's step method is not used and the robber does not move.
        Otherwise, the new positions are written to __robber_positions.
        At the end of the step, we check whether the robber is at one of the cop positions where the cop did not move.
        In this case, the cops win.
        """
        robber_position: int | None = None
        start_time: float = time.process_time()
        try:
            robber_position = self.robber.step(self.__cop_positions)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__robber_exception()
        end_time: float = time.process_time()
        if not self.__is_step_timeout(start_time, end_time):
            self.__set_robber_position(robber_position)
        if self.__cop_positions is not None and self.__robber_position in self.__cop_positions:
            self.__robber_caught()

    def __cops_step(self) -> None:
        """Computes a single cops step by calling the cops' step method.

        We track the time needed to compute the step, and we also look out for exceptions.
        In case of an exception, the exception is logged and the robber wins.
        In case of a timeout, the result of the cops' step method is not used and the cops do not move.
        Otherwise, the new positions are written to __cop_positions.
        """
        cop_positions: list[int] | None = None
        start_time: float = time.process_time()
        try:
            cop_positions = self.cops.step(self.__robber_position)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__cops_exception()
        end_time: float = time.process_time()
        if not self.__is_step_timeout(start_time, end_time):
            self.__set_cop_positions(cop_positions)
        else:
            self.__set_cop_positions(self.__cop_positions)

    def round_number(self) -> int:
        """Returns the number of the current round, i.e. the round that has just been played.

        :return: the number of the current round
        """
        return self.__round_number

    def result(self) -> int:
        """Returns the current result of the game.

        :return: 0 if the game continues, -1 if the robber wins, and 1 if the cops win
        """
        return self.__result

    def status(self) -> str:
        """Returns the current status of the game.

        Possible values are
        - Game continues
        - Cops timeout
        - Robber timeout
        - Cops invalid step
        - Robber invalid step
        - Exception in cops call
        - Exception in robber call
        - Robber caught
        """
        return self.__status

    def next_player(self) -> str:
        """Returns the next player.

        :return: the next player
        """
        return self.__next_player

    def cop_positions(self) -> list[int]:
        """Returns the current cop positions.

        :return: the current cop positions
        """
        if self.__cop_positions is not None:
            return self.__cop_positions
        return []

    def robber_position(self) -> int:
        """Returns the current robber position.

        :return: the current robber position
        """
        return self.__robber_position

    def __is_init_timeout(self, start_time: float, end_time: float) -> bool:
        """Checks whether the time between start_time and end_time exceeds the timeout given in self.timeout_init.

        :param start_time: the start of the period
        :param end_time: the end of the period
        :return: True if the timeout is exceeded and False otherwise
        """
        return self.timeout_init is not None and end_time - start_time > self.timeout_init

    def __is_step_timeout(self, start_time: float, end_time: float) -> bool:
        """Checks whether the time between start_time and end_time exceeds the timeout given in self.timeout_step.

        :param start_time: the start of the period
        :param end_time: the end of the period
        :return: True if the timeout is exceeded and False otherwise
        """
        return self.timeout_step is not None and end_time - start_time > self.timeout_step

    def __set_cop_positions(self, positions: list[int]) -> None:
        """Updates the current and previous cop positions if the given ones are valid

        If the given list of positions is None and no positions are set, then the robber wins.
        If there are already positions set and the given positions are None, nothing changes.
        If the list of given positions does not have the right length
        or of the given positions is not in the graph,
        the robber wins, i.e. __result is set to -1.
        :param positions: the next cop positions
        """
        if positions is None:
            if self.__cop_positions is None:
                self.__cops_invalid_step()
            return
        if self.cops_count is None:
            self.cops_count = len(positions)
        elif len(positions) != self.cops_count:
            self.__cops_invalid_step()
            return
        for pos in positions:
            if pos not in self.graph:
                self.__cops_invalid_step()
                return
        if self.__cop_positions:
            self.__prev_cop_positions = list(self.__cop_positions)
        self.__cop_positions = positions

    def __set_robber_position(self, position: int) -> None:
        """Updates the current robber position if the given one is valid.

        If the given position is None and no position is set, then the cops win.
        If there is already a position set and the given position is None, nothing changes.
        If the given position is not in the graph or not a valid robber move,
        cops win, i.e. __result is set to 1.
        :param position: the next robber positions
        """
        if position is None:
            if self.__robber_position is None:
                self.__robber_invalid_step()
            return
        if position not in self.graph:
            self.__robber_invalid_step()
            return
        if self.__robber_position is not None and position != self.__robber_position:
            graph = nx.Graph(self.graph)
            for idx in range(self.cops_count):
                if self.__cop_positions[idx] == self.robber_position():
                    continue
                if self.__cop_positions[idx] == self.__prev_cop_positions[idx] \
                        and self.__cop_positions[idx] in graph:
                    graph.remove_node(self.__cop_positions[idx])
            if not graph.has_node(position) or not nx.has_path(graph, self.__robber_position, position):
                self.__robber_invalid_step()
        self.__robber_position = position

    def __robber_caught(self) -> None:
        """If the robber is caught, the cops win and the status of the game is set to 'Robber caught'."""
        self.__cops_win()
        self.__set_status('Robber caught')

    def __cops_invalid_step(self) -> None:
        """If the cops make an invalid step, robber wins and the status of the game is set to 'Cops invalid step.'"""
        self.__robber_win()
        self.__set_status('Cops invalid step')

    def __robber_invalid_step(self) -> None:
        """If robber makes an invalid step, the cops win and the status of the game is set to 'Robber invalid step.'"""
        self.__cops_win()
        self.__set_status('Robber invalid step')

    def __cops_timeout(self) -> None:
        """If one of the cop calls has a timeout, robber wins and the status of the game is set to 'Cops timeout'."""
        self.__robber_win()
        self.__set_status('Cops timeout')

    def __robber_timeout(self) -> None:
        """If one of the robber calls has a timeout, cops win and the status of the game is set to 'Robber timeout'."""
        self.__cops_win()
        self.__set_status('Robber timeout')

    def __cops_exception(self) -> None:
        """If an exception is raised in one of the cop calls, robber wins and the status of the game is set to
        'Exception in cops call'.
        """
        self.__robber_win()
        self.__set_status('Exception in cops call')

    def __robber_exception(self) -> None:
        """If an exception is raised in one of the robber calls, the cops win and the status of the game is set to
        'Exception in robber call'.
        """
        self.__cops_win()
        self.__set_status('Exception in robber call')

    def __robber_win(self) -> None:
        """Sets __result to -1 if the game still continues."""
        if self.__result == 0:
            self.__result = -1

    def __cops_win(self) -> None:
        """Sets __result to 1 if the game still continues."""
        if self.__result == 0:
            self.__result = 1

    def __set_status(self, status: str) -> None:
        """Sets __status to the given value if the game still continues."""
        if self.__status == 'Game continues':
            self.__status = status

"""
# NOT NECESSARY, JUST FOR CONVENIENCE SO WE DONT NEED TO CHANGE THE IMPORTED PACKAGE IN current/game
if __name__ == '__main__':
    #graph_path: str = '../../graphs/i7-v2-4-robberwin-petersen.json'
    graph_path: str = '../../graphs/2-components-graph.json'
    max_rounds: int = 20
    logging.basicConfig(filename='run.log', level=logging.INFO)
    main_logger = logging.getLogger('main')
    main_graph: nx.Graph
    with open(graph_path) as jsonfile:
        main_graph = json_graph.node_link_graph(json.load(jsonfile))

    game = None
    result = 0
    round_number = 0
    game_status = None
    cops_count = 4

    while result == 0 and round_number <= max_rounds:
        if game is None:
            game = Game(main_graph, Try_catch_current, Try_catch_current, cops_count, 5, 0.5)
        else:
            game.step()
        round_number = game.round_number()
        result = game.result()
        game_status = game.status()
        main_logger.info('Cops positions: {cop_positions}, robber_position: {robber_position}'.format(
            cop_positions=game.cop_positions(),
            robber_position=game.robber_position())
        )
    main_logger.info('Result: {result}, status: {status}'.format(result=result, status=game_status))
    if round_number > max_rounds or result == -1:  # robber wins
        main_logger.info('Robber wins')
    else:
        main_logger.info('Cops win')
"""

if __name__ == '__main__':
    GRAPHPATH = '../../graphs/'
    graphs = [join(GRAPHPATH, f) for f in listdir(GRAPHPATH) if isfile(join(GRAPHPATH, f))]

    max_rounds: int = 20
    logging.basicConfig(filename='run.log', level=logging.INFO)
    main_logger = logging.getLogger('main')
    for graph in graphs:
        main_logger.info('')
        main_logger.info('')
        main_logger.info('#########################################')
        main_logger.info(graph)
        main_logger.info('#########################################')
        with open(graph) as jsonfile:
            main_graph = json_graph.node_link_graph(json.load(jsonfile))

        game = None
        result = 0
        round_number = 0
        game_status = None
        cops_count = 3

        while result == 0 and round_number <= max_rounds:
            if game is None:
                game = Game(main_graph, Try_catch_current, Try_catch_current, cops_count, 5, 0.5)
            else:
                game.step()
            round_number = game.round_number()
            result = game.result()
            game_status = game.status()
            main_logger.info('Cops positions: {cop_positions}, robber_position: {robber_position}'.format(
                cop_positions=game.cop_positions(),
                robber_position=game.robber_position())
            )
        main_logger.info('Result: {result}, status: {status}'.format(result=result, status=game_status))
        if round_number > max_rounds or result == -1:  # robber wins
            main_logger.info('Robber wins')
        else:
            main_logger.info('Cops win')
